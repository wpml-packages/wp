<?php

namespace WPML\LIB\WP;


use tad\FunctionMocker\FunctionMocker;

trait HooksMock {

	protected function setupHooksMock() {
		FunctionMocker::replace( Hooks::class . '::callWithFilter', function ( $fn, $name, $filterFn, $priority = 10, $count = 1 ) {
			return $fn();
		} );
	}
}
