<?php

namespace WPML\LIB\WP;

use WPML\FP\Obj;

trait PostTypeMock {

	private $postsCounts;

	public $postTypes;

	public function setUpPostTypeMock() {
		$this->postsCounts = [];

		$postType         = \Mockery::mock( 'WP_Post_Type' );
		$postType->labels = [ 'name' => 'Posts', 'singular_name' => 'Post' ];

		$pageType         = \Mockery::mock( 'WP_Post_Type' );
		$pageType->labels = [ 'name' => 'Pages', 'singular_name' => 'Page' ];

		$this->postTypes = [
			'post' => $postType,
			'page' => $pageType,
		];

		\WP_Mock::userFunction( 'wp_count_posts', [
			'return' => function ( $postType ) {
				return (object) Obj::objOf( 'publish', Obj::propOr( 0, $postType, $this->postsCounts ) );
			}
		] );

		\WP_Mock::userFunction( 'get_post_type_object', [
			'return' => function ( $postType ) {
				return Obj::propOr( null, $postType, $this->postTypes );
			}
		] );

		PostType::init();
	}

	public function mockPublishedPostsCount( $postType, $count ) {
		$this->postsCounts[ $postType ] = $count;
	}
}
