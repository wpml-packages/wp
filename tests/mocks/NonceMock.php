<?php


namespace WPML\LIB\WP;

use tad\FunctionMocker\FunctionMocker;

trait NonceMock {

	public function setUpNonce() {
		FunctionMocker::replace( 'wp_create_nonce', function( $action ) {
			return md5( $action );
		});
		FunctionMocker::replace( 'wp_verify_nonce', function( $nonce, $action ) {
			return $nonce === md5( $action );
		});
	}

}
