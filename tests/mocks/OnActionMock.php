<?php


namespace WPML\LIB\WP;

use tad\FunctionMocker\FunctionMocker;
use WPML\FP\Logic;
use WPML\FP\Obj;
use WPML\FP\Relation;

trait OnActionMock {

	private $actions = [];
	private $filters = [];

	public function setUpOnAction() {
		FunctionMocker::setUp();
		FunctionMocker::replace( 'wpml_test_add_action', function ( $action, $callback, $priority = 10, $accepted_args = 1 ) {
			$this->actions[] = [
				'action'        => $action,
				'callback'      => $callback,
				'priority'      => $priority,
				'accepted_args' => $accepted_args,
			];
		} );
		FunctionMocker::replace( Hooks::class . '::onAction', function ( $action, $priority = 10, $accepted_args = 1 ) {
			return Hooks::onHook( 'wpml_test_add_action', $action, $priority, $accepted_args );
		} );

		FunctionMocker::replace( 'wpml_test_add_filter', function ( $filter, $callback, $priority = 10, $accepted_args = 1 ) {
			$this->filters[] = [
				'filter'        => $filter,
				'callback'      => $callback,
				'priority'      => $priority,
				'accepted_args' => $accepted_args,
			];
		} );
		FunctionMocker::replace( Hooks::class . '::onFilter', function ( $action, $priority = 10, $accepted_args = 1 ) {
			return Hooks::onHook( 'wpml_test_add_filter', $action, $priority, $accepted_args );
		} );
	}

	public function tearDownOnAction() {
		FunctionMocker::tearDown();
	}

	/**
	 * @param  string $actionName
	 * @param  int    $priority
	 * @param  int    $accepted_args
	 *
	 * @return bool
	 */
	public function assertActionAdded( $actionName, $priority = 10, $accepted_args = 1 ) {
		return $this->assertNotEmpty( wpml_collect( $this->actions )
			->filter(
				Logic::allPass(
					[
						Relation::propEq( 'action', $actionName ),
						Relation::propEq( 'priority', $priority ),
						Relation::propEq( 'accepted_args', $accepted_args ),
					]
				)
			)->toArray() );
	}

	/**
	 * @param  string $filterName
	 * @param  int    $priority
	 * @param  int    $accepted_args
	 *
	 * @return bool
	 */
	public function assertFilterAdded( $filterName, $priority = 10, $accepted_args = 1 ) {
		return $this->assertNotEmpty( wpml_collect( $this->filters )
			->filter(
				Logic::allPass(
					[
						Relation::propEq( 'filter', $filterName ),
						Relation::propEq( 'priority', $priority ),
						Relation::propEq( 'accepted_args', $accepted_args ),
					]
				)
			)->toArray() );
	}

	/**
	 * @param  string $actionName
	 *
	 * @return bool
	 */
	public function assertActionNotAdded( $actionName ) {
		return $this->assertEmpty( wpml_collect( $this->actions )
			->filter( Relation::propEq( 'action', $actionName ) )
			->toArray() );
	}

	/**
	 * @param string|null $actionName
	 */
	public function runAction( $actionName = null ) {
		$args = array_slice( func_get_args(), 1 );

		if ( ! $actionName ) {
			$this->assertCount( 1, $this->actions );
			$actionName = current( array_keys( $this->actions ) );
		}

		wpml_collect( $this->actions )
			->filter( Relation::propEq( 'action', $actionName ) )
			->sort( function( $a, $b ) {
				return Relation::lt(
					Obj::prop( 'priority', $a ),
					Obj::prop( 'priority', $b )
				);
			} )
			->map( function( $callback ) use ( $args ) {
				call_user_func_array( $callback['callback'], $args );
			} );
	}

	/**
	 * @param  string|null $filterName
	 *
	 * @return mixed
	 */
	public function runFilter( $filterName = null ) {
		$args = array_slice( func_get_args(), 1 );

		if ( ! $filterName ) {
			$this->assertCount( 1, $this->filters );
			$filterName = current( array_keys( $this->filters ) );
		}

		wpml_collect( $this->filters )
			->filter( Relation::propEq( 'filter', $filterName ) )
			->sort( function( $a, $b ) {
				return Relation::lt(
					Obj::prop( 'priority', $a ),
					Obj::prop( 'priority', $b )
				);
			} )
			->map( function( $callback ) use ( &$args ) {
				$args[0] = call_user_func_array( $callback['callback'], $args );
			} );

		return Obj::propOr( null, 0, $args );
	}

}
