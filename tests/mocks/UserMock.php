<?php

namespace WPML\LIB\WP;

use tad\FunctionMocker\FunctionMocker;
use WPML\FP\Fns;
use WPML\FP\Obj;
use WPML\FP\Str;

trait UserMock {

	private $currentUser;

	private $userMeta;

	private $caps;

	private $notifications;

	private $insertShouldFail;

	public function setUpUserMock() {
		$this->userMeta         = [];
		$this->caps             = [];
		$this->notifications    = [];
		$this->insertShouldFail = false;

		$this->currentUser = \Mockery::mock( 'WP_User' );
		$this->currentUser->shouldReceive( 'has_cap' )->andReturnUsing( function ( $cap ) {
			return Obj::propOr( false, $this->currentUser->ID . '-' . $cap, $this->caps );
		} );
		$this->currentUser->shouldReceive( 'add_cap' )->andReturnUsing( function ( $cap ) {
			$this->caps[ $this->currentUser->ID . '-' . $cap ] = true;
		} );
		$this->currentUser->shouldReceive( 'remove_cap' )->andReturnUsing( function ( $cap ) {
			unset( $this->caps[ $this->currentUser->ID . '-' . $cap ] );
		} );
		$this->currentUser->shouldReceive( 'get_role_caps' )->andReturn( [] );
		$this->currentUser->shouldReceive( 'exists' )->andReturnUsing( function () { return $this->currentUser->ID > 0; } );
		$this->currentUser->ID           = 1;
		$this->currentUser->locale       = 'en_US';
		$this->currentUser->display_name = 'admin';
		$this->currentUser->user_login   = 'admin';
		$this->currentUser->user_email   = 'admin@admin.com';

		\WP_Mock::userFunction( 'wp_get_current_user', [
			'return' => function () { return $this->currentUser; },
		] );

		\WP_Mock::userFunction( 'get_current_user_id', [ 'return' => function () { return $this->currentUser->ID; } ] );

		\WP_Mock::userFunction( 'update_user_meta', [
			'return' => function ( $userId, $metaKey, $metaValue ) {
				$this->userMeta[ $userId . '-' . $metaKey ] = $metaValue;
			}
		] );

		\WP_Mock::userFunction( 'get_user_meta', [
			'return' => function ( $userId, $metaKey, $single ) {
				$value = Obj::propOr( '', $userId . '-' . $metaKey, $this->userMeta );

				return $single ? $value : [ $value ];
			}
		] );

		\WP_Mock::userFunction( 'delete_user_meta', [
			'return' => function ( $userId, $metaKey ) {
				unset( $this->userMeta[ $userId . '-' . $metaKey ] );

				return true;
			}
		] );

		\WP_Mock::userFunction( 'get_editable_roles', [ 'return' => [] ] );

		FunctionMocker::replace( User::class . '::get', function ( $userId = null ) {
			$update = function ( $userId ) {
				// NOTE: we are reusing the current user for convenience. This may need
				// to be changed if tests need to use the current user and a different
				// user in the future.
				$this->mockCurrentUserId( $userId );

				return $this->currentUser;
			};

			return $userId === null ? $update : $update( $userId );
		} );

		\WP_Mock::userFunction( 'wp_generate_password', [ 'return' => 'password' ] );

		\WP_Mock::userFunction( 'wp_insert_user', [
			'return' => function ( $data ) {
				if ( $this->insertShouldFail ) {
					$error = \Mockery::mock( '\WP_Error' );
					$error->shouldReceive( 'get_error_messages' )->withNoArgs()->andReturn(
						[ 'The user could not be created' ]
					);

					return $error;
				} else {
					// NOTE: we are reusing the current user for convenience. This may need
					// to be changed if tests need to use the current user and a different
					// user in the future.
					return $this->currentUser->ID;
				}
			}
		] );

		\WP_Mock::userFunction( 'wp_send_new_user_notifications', [
			'return' => function ( $userId ) {
				$this->notifications[ $userId . '-new' ] = true;
			}
		] );

		\WP_Mock::userFunction( 'get_avatar', [
			'return' => Str::concat( 'avatar-' )
		] );

		\WP_Mock::userFunction( 'get_avatar_url', [
			'return' => Str::concat( 'avatar-url-' )
		] );

	}

	public function mockCurrentUserId( $userID ) {
		$this->currentUser->ID = $userID;
	}

	public function mockInsertShouldFail( $state ) {
		$this->insertShouldFail = $state;
	}

	public function assertNotifiedNew( $userId ) {
		return Obj::propOr( false, $userId . '-new', $this->notifications );
	}
}
