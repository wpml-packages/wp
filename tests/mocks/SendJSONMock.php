<?php


namespace WPML\LIB\WP;

use tad\FunctionMocker\FunctionMocker;
use WPML\FP\Either;

trait SendJSONMock {

	private $errorData;
	private $successData;

	public function setUpSendJSON() {
		FunctionMocker::replace( 'wp_send_json_error', function ( $data ) {
			$this->errorData = $data;
		} );
		FunctionMocker::replace( 'wp_send_json_success', function ( $data ) {
			$this->successData = $data;

			return Either::right( $data ); // This is needed so "catch" is not called
		} );
	}

	public function assertErrorSent( $data ) {
		$this->assertEquals( $data, $this->errorData );
	}

	public function assertSuccessSent( $data ) {
		$this->assertEquals( $data, $this->successData );
	}

}
