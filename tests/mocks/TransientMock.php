<?php

namespace mocks;

use WPML\FP\Obj;
use WPML\LIB\WP\Transient;

trait TransientMock {

	private $transients;

	public function setUpTransientMock() {
		$this->transients = [];

		\WP_Mock::userFunction( 'get_transient', [
			'return' => function ( $key ) {
				return Obj::prop( $key, $this->transients );
			}
		] );

		\WP_Mock::userFunction( 'delete_transient', [
			'return' => function ( $key ) {
				unset( $this->transients[ $key ] );
			}
		] );

		\WP_Mock::userFunction( 'set_transient', [
			'return' => function ( $key, $data, $expiration ) {
				$this->transients[ $key ] = $data;
			}
		] );

		Transient::init();
	}

}
