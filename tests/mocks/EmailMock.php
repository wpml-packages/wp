<?php

namespace WPML\LIB\WP;


use WPML\FP\Fns;
use WPML\FP\Obj;
use WPML\FP\Str;

trait EmailMock {

	private $emails;

	protected function setupEmailMock() {
		$this->emails = [];

		\WP_Mock::userFunction( 'wp_mail', [
				'return' => function ( $to, $subject, $message, $headers ) {
					$this->emails[ $to ] = [ 'subject' => $subject, 'message' => $message, 'headers' => $headers ];
				}
			]
		);
	}

	private function assertEmailSentTo( $email ) {
		$this->assertCount( 1, Fns::filter( Str::includes( $email ), Obj::keys( $this->emails ) ) );
	}
}
