<?php

namespace WPML\LIB\WP;

trait OptionMock {

	private $options;

	public function setUpOptionMock() {

		$this->options  = [];

		\WP_Mock::userFunction( 'get_option', [
			'return' => function ( $name, $default = false ) {
				return array_key_exists( $name, $this->options ) ? $this->options[ $name ]['value'] : $default;
			}
		] );

		\WP_Mock::userFunction( 'update_option', [
			'return' => function ( $name, $value, $autoload = true ) {
				$newValue = [
					'value'    => $value,
					'autoload' => $autoload,
				];

				if (
					// Same as existing value
					array_key_exists( $name, $this->options )
					&& $this->options[ $name ] == $newValue
				) {
					return false;
				}

				$this->options[ $name ] = $newValue;
				return true;
			}
		] );

		\WP_Mock::userFunction( 'delete_option', [
			'return' => function ( $name ) {
				unset( $this->options[ $name ] );
				return true;
			}
		] );

		Option::init();
	}
}
