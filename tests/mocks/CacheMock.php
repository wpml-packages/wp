<?php

namespace WPML\LIB\WP;

use tad\FunctionMocker\FunctionMocker;
use WPML\FP\Lst;
use WPML\FP\Maybe;
use WPML\FP\Obj;

trait CacheMock {

	private $cache;

	public function setUpCache() {
		$this->cache = [];

		FunctionMocker::replace( Cache::class . '::getInternal', function ( $group, $key ) {
			if ( array_key_exists( $group, $this->cache ) && array_key_exists( $key, $this->cache[ $group ] ) ) {
				return Maybe::just( $this->cache[ $group ][ $key ]['data'] );
			}

			return Maybe::nothing();
		} );

		\WP_Mock::userFunction( 'wp_cache_set', [ 'return' => function ( $key, $value, $group ) {
				$this->cache[ $group ][ $key ] = $value;

				return true;
			}
		] );

		\WP_Mock::userFunction( 'wp_cache_delete', [ 'return' => function ( $key, $group ) {
				if ( isset( $this->cache[ $group ] ) && isset( $this->cache[ $group ][ $key ] ) ) {
					unset( $this->cache[ $group ][ $key ] );
				}
			}
		] );
	}

}
