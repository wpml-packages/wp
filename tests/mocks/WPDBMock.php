<?php


namespace WPML\LIB\WP;


use WPML\FP\Fns;
use WPML\FP\Logic;
use WPML\FP\Lst;
use WPML\FP\Maybe;
use WPML\FP\Relation;
use WPML\FP\Str;
use function WPML\FP\curryN;
use function WPML\FP\pipe;

trait WPDBMock {

	private $wpdbPrepareHandlers;
	private $wpdbUpdateHandlers;
	private $wpdbInsertHandlers;
	private $wpdbDeleteHandlers;
	private $wpdbQueryHandlers;
	private $wpdbReplaceHandlers;

	public function setUpWPDBMock() {
		global $wpdb;

		$this->wpdbPrepareHandlers = [];
		$this->wpdbUpdateHandlers  = [];
		$this->wpdbInsertHandlers  = [];
		$this->wpdbDeleteHandlers  = [];
		$this->wpdbQueryHandlers   = [];

		$wpdb                  = \Mockery::mock( 'wpdb' );
		$wpdb->posts           = 'wp_posts';
		$wpdb->postmeta        = 'wp_postmeta';
		$wpdb->options         = 'wp_options';
		$wpdb->prefix          = 'wp_';
		$wpdb->suppress_errors = false;

		\WP_Mock::userFunction( 'wpml_prepare_in', [
			'return' => pipe( Fns::map( Logic::ifElse( 'is_numeric', Fns::identity(), Str::wrap( '"', '"' ) ) ), Lst::join( ', ' ) )
		] );

		$wpdb->shouldReceive( 'get_results' )->andReturnUsing( Fns::identity() );
		$wpdb->shouldReceive( 'get_var' )->andReturnUsing( Fns::identity() );
		$wpdb->shouldReceive( 'get_col' )->andReturnUsing( Fns::identity() );
		$wpdb->shouldReceive( 'get_row' )->andReturnUsing( Fns::identity() );

		$execCallback = curryN( 2, function ( $fn, array $args ) {
			return call_user_func_array( $fn, $args );
		} );

		$execFirstMatching = function ( $handlers, $args ) use ( $execCallback ) {
			return Maybe::of( $handlers )
			            ->map( Lst::find( pipe( Lst::nth( 0 ), $execCallback( Fns::__, $args ) ) ) )
			            ->map( Lst::nth( 1 ) )
			            ->map( $execCallback( Fns::__, $args ) )
			            ->getOrElse( null );
		};

		$wpdb->shouldReceive( 'prepare' )->andReturnUsing( function ( $sql, ...$args ) use ( $execFirstMatching ) {
			return $execFirstMatching( $this->wpdbPrepareHandlers, func_get_args() );
		} );

		$wpdb->shouldReceive( 'update' )->andReturnUsing( function ( $table, $data, $where ) use ( $execFirstMatching ) {
			return $execFirstMatching( $this->wpdbUpdateHandlers, func_get_args() );
		} );

		$wpdb->shouldReceive( 'delete' )->andReturnUsing( function ( $table, $where, $format ) use ( $execFirstMatching ) {
			return $execFirstMatching( $this->wpdbDeleteHandlers, func_get_args() );
		} );

		$wpdb->shouldReceive( 'insert' )->andReturnUsing( function ( $table, $data ) use ( $execFirstMatching ) {
			return $execFirstMatching( $this->wpdbInsertHandlers, func_get_args() );
		} );

		$wpdb->shouldReceive( 'query' )->andReturnUsing( function ( $query ) use ( $execFirstMatching ) {
			return $execFirstMatching( $this->wpdbQueryHandlers, func_get_args() );
		} );

		$wpdb->shouldReceive( 'replace' )->andReturnUsing( function ( $query ) use ( $execFirstMatching ) {
			return $execFirstMatching( $this->wpdbReplaceHandlers, func_get_args() );
		} );

		$wpdb->shouldReceive( 'suppress_errors' )->andReturnUsing( function ( $suppress_errors ) {
			global $wpdb;
			$wpdb->suppress_errors = $suppress_errors;
		} );
	}

	public function addPrepareHandler( $predicate, $fn ) {
		$this->wpdbPrepareHandlers[] = [ $predicate, $fn ];
	}

	public function addUpdateHandler( $predicate, $fn ) {
		$this->wpdbUpdateHandlers[] = [ $predicate, $fn ];
	}

	public function addDeleteHandler( $predicate, $fn ) {
		$this->wpdbDeleteHandlers[] = [ $predicate, $fn ];
	}

	public function addInsertHandler( $predicate, $fn ) {
		$this->wpdbInsertHandlers[] = [ $predicate, $fn ];
	}

	public function addQueryHandler( $predicate, $fn ) {
		$this->wpdbQueryHandlers[] = [ $predicate, $fn ];
	}

	public function addReplaceHandler( $predicate, $fn ) {
		$this->wpdbReplaceHandlers[] = [ $predicate, $fn ];
	}

	public function tableEquals( $tableName ) {
		return pipe( Fns::nthArg( 0 ), Relation::equals( $tableName ) );
	}

	public function getQuery() {
		return Fns::nthArg( 0 );
	}

	public function getTrimmedQuery() {
		return pipe( $this->getQuery(), Fns::unary( 'trim' ) );
	}

	public function queryContains( $needle ) {
		return pipe( $this->getQuery(), Str::includes( $needle ) );
	}

	public function queryEquals( $query ) {
		return pipe( $this->getTrimmedQuery(), Relation::equals( $query ) );
	}

}
