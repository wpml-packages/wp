<?php

namespace WPML\LIB\WP;

use WPML\FP\Fns;
use WPML\FP\Logic;
use WPML\FP\Lst;
use WPML\FP\Obj;
use WPML\FP\Str;

trait PostMock {

	private $postMeta;
	private $postTypes;
	private $posts;
	private $postStatuses;

	public function setUpPostMock() {
		$this->postMeta     = [];
		$this->postTypes    = [];
		$this->posts        = [];
		$this->postStatuses = [];

		\WP_Mock::userFunction( 'get_the_terms', [ 'args' => [], 'return' => null ] );

		\WP_Mock::userFunction( 'update_post_meta', [
			'return' => function ( $postId, $metaKey, $value ) {
				$this->postMeta[ $postId . '-' . $metaKey ] = $value;
			}
		] );

		\WP_Mock::userFunction( 'get_post_meta', [
			'return' => function ( $postId, $metaKey = '', $single = false ) {
				if ( $metaKey ) {
					// if the value is not defined, return an empty string by default as it is done in WordPress
					return Obj::propOr( '', $postId . '-' . $metaKey, $this->postMeta );
				} else {
					$metaKeys = Obj::keys( $this->postMeta );
					$metaKeys = Fns::filter( Str::startsWith( $postId . '-'), $metaKeys );
					$result = [];
					foreach ( $metaKeys as $metaKey ) {
						$result[ Str::trimPrefix( $postId . '-', $metaKey) ] = $this->postMeta[ $metaKey ];
					}
					return $result;
				}
			}
		] );

		\WP_Mock::userFunction( 'delete_post_meta', [
			'return' => function ( $postId, $metaKey, $metaValue = '' ) {
				if ( isset( $this->postMeta[ $postId . '-' . $metaKey ] ) ) {
					unset( $this->postMeta[ $postId . '-' . $metaKey ] );

					return true;
				}

				return false;
			}
		] );

		\WP_Mock::userFunction( 'get_post_type', [
			'return' => function ( $postId ) {
				return Obj::propOr( false, $postId, $this->postTypes );
			}
		] );

		\WP_Mock::userFunction( 'get_post', [
			'return' => function ( $postId ) {
				return Obj::prop( $postId, $this->posts );
			}
		] );

		\WP_Mock::userFunction( 'get_post_status', [
			'return' => function ( $postId ) {
				return Obj::propOr( false, $postId, $this->postStatuses );
			}
		] );

		\WP_Mock::userFunction( 'wp_update_post', [
			'return' => function ( $data ) {
				$id = Obj::propOr( 0, 'ID', $data );
				if ( isset( $this->posts[ $id ] ) ) {
					foreach ( $data as $field => $val ) {
						$this->posts[ $id ] = Obj::assoc( $field, $val, $this->posts[ $id ] );
						if ( $field === 'post_status' ) {
							$this->mockPostStatus( $id, $val );
						}
					}

					return $id;
				}

				return 0;
			}
		] );

		\WP_Mock::userFunction( 'wp_insert_post', [
			'return' => function ( $data ) {
				$id                 = Logic::isEmpty( $this->posts ) ? 1 : max( Lst::pluck( 'ID', $this->posts ) ) + 1;
				$this->posts[ $id ] = $this->createPost(
					$id,
					Obj::prop( 'post_title', $data ),
					Obj::prop( 'post_excerpt', $data ),
					Obj::prop( 'post_content', $data ),
					Obj::prop( 'post_status', $data ),
					Obj::prop( 'post_type', $data ),
					Obj::prop( 'post_author', $data )
				);

				return $id;
			}
		] );

		\WP_Mock::userFunction( 'wp_delete_post', [
			'return' => function ( $postId, $forceDelete ) {
				if ( ! $forceDelete && Obj::has( $postId, $this->posts ) ) {
					$this->postStatuses[ $postId ] = 'trash';
				} else {
					$this->posts = Obj::removeProp( $postId, $this->posts );
				}
			}
		] );

		$this->addUpdateHandler(
			function ( $table, $data, $where ) {
				global $wpdb;

				return $table === $wpdb->posts && isset( $where['ID'] );
			},
			function ( $table, $data, $where ) {
				$data['ID'] = $where['ID'];

				return (bool) \wp_update_post( $data );
			}
		);

		Post::init(); // Make sure we use these mocked functions.
	}

	public function setTermsToPost( $postId, $taxonomy, $terms ) {
		\WP_Mock::userFunction( 'get_the_terms', [
			'args'   => [ $postId, $taxonomy ],
			'return' => $terms,
		] );
	}

	public function mockPostType( $postId, $type ) {
		$this->postTypes[ $postId ] = $type;
	}

	public function mockPost( $postId, $data = null ) {
		$this->posts[ $postId ] = $data ?: $this->createPost( $postId );
	}

	public function createPost( $id, $title = null, $excerpt = null, $content = null, $status = null, $type = null, $post_author = null ) {
		$post               = $this->getMockBuilder( '\WP_Post' )->getMock();
		$post->ID           = $id;
		$post->post_title   = $title ?: 'Title #' . $id;
		$post->post_excerpt = $excerpt ?: 'Excerpt #' . $id;
		$post->post_content = $content ?: 'Content #' . $id;
		$post->post_status  = $status ?: 'publish';
		$post->post_type    = $type ?: 'page';
		$post->post_author  = $post_author ?: 123;

		$this->mockPostStatus( $id, $status );

		return $post;
	}

	public function mockPostStatus( $postId, $status ) {
		$this->postStatuses[ $postId ] = $status;
	}
}
