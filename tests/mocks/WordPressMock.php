<?php


namespace mocks;


trait WordPressMock {
	
	public function setUpWordpressMock() {
		
		$this->getMockBuilder( '\WP_Error' )->getMock();
		\WP_Mock::userFunction( 'is_wp_error', [
			'return' => function ( $param ) {
				return $param instanceof \WP_Error;
			}
		] );
		
		\WP_Mock::userFunction( 'admin_url', [
				'return' => '/wp-admin'
			]
		);
		
		\WP_Mock::userFunction( 'wp_login_url', [
				'return' => '/wp-login.php'
			]
		);
	}
	
}