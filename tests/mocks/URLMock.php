<?php

namespace WPML\LIB\WP;

use WPML\FP\Str;

trait URLMock {

	private $currentUrl = 'http://current_url/';

	public function setUpURLMocks() {
		\WP_Mock::userFunction( 'get_permalink', [ 'return' => Str::concat( 'http://develop/fr/?page_id=' ) ] );

		\WP_Mock::userFunction( 'add_query_arg', [
			'return' => function ( $args, $url = null ) {
				if ( ! $url ) {
					$url = $this->currentUrl;
				}

				$params = [];
				foreach ( $args as $key => $val ) {
					$params[] = $key . '=' . $val;
				}

				if ( $params ) {
					if ( strpos( $url, '?' ) ) {
						$url .= '&' . implode( '&', $params );
					} else {
						$url .= '?' . implode( '&', $params );
					}
				}

				return $url;
			}
		] );

		\WP_Mock::userFunction( 'remove_query_arg', [
			'return' => function ( $args, $url = null ) {
				if ( ! $url ) {
					$url = $this->currentUrl;
				}

				foreach ( $args as $val ) {
					$url = Str::replace( '&' . $val, '', $url );
				}

				return $url;
			}
		] );

		\WP_Mock::userFunction( 'wp_parse_url', [
			'return' => function ( $url ) {
				return parse_url( $url );
			}
		] );

		\WP_Mock::userFunction( 'get_admin_url', [ 'return' => 'http://develop.test/wp-admin' ] );
	}

}