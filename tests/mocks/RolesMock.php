<?php

namespace WPML\LIB\WP;

trait RolesMock {

	public function setupRolesMock() {
		global $wp_roles;
		$wp_roles        = \Mockery::mock( 'WP_Roles' );
		$wp_roles->roles = [
			'administrator' => [ 'capabilities' => [ 'manage_options' => true ] ],
			'subscriber'    => [ 'capabilities' => [ 'manage_options' => false ] ],
		];
	}
}
