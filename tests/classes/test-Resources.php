<?php

namespace WPML\LIB\WP\App;

use tad\FunctionMocker\FunctionMocker;
use WPML\FP\Fns;
use WPML\FP\Obj;
use WPML\LIB\WP\Nonce;
use WPML\LIB\WP\NonceMock;

class Test_Resources extends \OTGS_TestCase {

	use NonceMock;

	public function setUp() {
		parent::setUp();

		FunctionMocker::replace( 'file_exists', true );
	}

	/**
	 * @test
	 * @dataProvider dpLocalizeAndDomain
	 */
	public function it_enqueues_resources( $localizeData, $domain, $wp_version_to_set, $has_wp_set_script_translations_function ) {
		global $wp_version;
		$wp_version = $wp_version_to_set;

		$app            = 'some-app';
		$pluginBaseUrl  = 'http://some-site.com/wp-content/plugins/xyz';
		$pluginBasePath = '/www/site/wp-content/plugins/xyz';
		$version        = '1.2.3';

		\WP_Mock::userFunction(
			'wp_register_script',
			[
				'times' => 1,
				'args'  => [
					"wpml-{$app}-ui",
					$pluginBaseUrl . "/dist/js/{$app}/app.js",
					array(),
					$version
				]
			]
		);

		$expectedData = Obj::prop( 'data', $localizeData );
		if ( $expectedData ) {
			$expectedData['nonce']     = Nonce::create( Obj::prop( 'endpoint', $expectedData ) );
			$expectedData['endpoints'] = Fns::map(
				function ( $endpoint ) {
					return [
						'endpoint' => $endpoint,
						'nonce'    => Nonce::create( $endpoint )
					];
				},
				$expectedData['endpoints']
			);
		}
		\WP_Mock::userFunction(
			'wp_localize_script',
			[
				'times' => $localizeData ? 1 : 0,
				'args'  => [
					"wpml-{$app}-ui",
					Obj::prop( 'name', $localizeData ),
					$expectedData,
				]
			]
		);

		\WP_Mock::userFunction(
			'wp_enqueue_script',
			[
				'times' => 1,
				'args'  => [
					"wpml-{$app}-ui",
				]
			]
		);

		\WP_Mock::userFunction(
			'wp_enqueue_style',
			[
				'times' => 1,
				'args'  => [
					"wpml-{$app}-ui",
					$pluginBaseUrl . "/dist/css/{$app}/styles.css",
					array(),
					$version
				]
			]
		);

		\WP_Mock::userFunction(
			'wp_set_script_translations',
			[
				'times' => $domain && $has_wp_set_script_translations_function ? 1 : 0,
				'args'  => [
					"wpml-{$app}-ui",
					$domain,
					$pluginBasePath . "/locale/jed",
				]
			]
		);

		Resources::enqueue(
			$app, $pluginBaseUrl, $pluginBasePath, $version, $domain, $localizeData
		);
	}

	public function dpLocalizeAndDomain() {
		$localizeData = [
			'name' => 'something',
			'data' => [
				'endpoint'  => 'MYENDPOINT',
				'endpoints' => [ 'key1' => 'MYENDPOINT1', 'key2' => 'MYENDPOINT2' ],
				'data'      => 'xyz'
			]
		];
		$domain       = 'some-domain';

		return [
			'with both'                           => [ $localizeData, $domain, '5.0.0', true ],
			'without domain'                      => [ $localizeData, null, '5.3.0', true ],
			'without localized data'              => [ null, $domain, '5.2.1', true ],
			'with both - WP < 5.0.0'              => [ $localizeData, $domain, '4.9.9', false ],
			'without domain - WP < 5.0.0'         => [ $localizeData, null, '4.9.0', false ],
			'without localized data - WP < 5.0.0' => [ null, $domain, '4.5.0', false ],
		];
	}
}
