<?php

namespace WPML\LIB\WP;

use WPML\FP\Left;
use WPML\FP\Right;
use WPML\FP\Wrapper;

class Test_Nonce extends \OTGS_TestCase {

	use NonceMock;

	public function setUp() {
		parent::setUp();

		$this->setUpNonce();
	}

	/**
	 * @test
	 */
	public function it_creates_nonce() {
		$action = 'my-action';
		$nonce  = 'some-nonce-value';

		\WP_Mock::userFunction( 'wp_create_nonce',
			[
				'times'  => 1,
				'args'   => [ $action ],
				'return' => $nonce,
			]
		);
		$this->assertEquals( $nonce, Nonce::create( $action ) );
	}

	/**
	 * @test
	 */
	public function it_returns_either_left_on_invalid_nonce() {
		$action = 'my-action';
		$nonce  = 'wrong-nonce-value';

		$result = Nonce::verify( $action, wpml_collect( [ 'nonce' => $nonce ] ) );
		$this->assertInstanceOf( Left::class, $result );
	}

	/**
	 * @test
	 */
	public function it_returns_either_right_on_valid_nonce() {
		$action = 'my-action';
		$nonce  = Nonce::create( $action );
		$postData = wpml_collect( [ 'nonce' => $nonce, 'other' => 'data' ] );

		$result = Nonce::verify( $action, $postData );

		$this->assertInstanceOf( Right::class, $result );
		$this->assertEquals( $postData, $result->get() );

	}

	/**
	 * @test
	 */
	public function it_returns_either_right_on_valid_endpoint() {
		$endpoint = 'some-end-point';
		$nonce  = Nonce::create( $endpoint );
		$postData = wpml_collect( [
			'nonce' => $nonce,
			'endpoint' => $endpoint,
			'other' => 'data'
		] );

		$result = Nonce::verifyEndPoint( $postData );

		$this->assertInstanceOf( Right::class, $result );
		$this->assertEquals( $postData, $result->get() );

	}

}
