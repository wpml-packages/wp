<?php

namespace WPML\LIB\WP;

use mocks\WordPressMock;

class Test_Url extends \OTGS_TestCase {
	
	use WordPressMock;
	
	public function setUp() {
		parent::setUp();
		
		$this->setUpWordpressMock();
	}
	
	/**
	 * @test
	 */
	public function test_admin_url() {
		$this->assertTrue( Url::isAdmin( 'http://wpml.tests/wp-admin' ) );
		
		$this->assertTrue( Url::isAdmin( 'http://wpml.tests/wp-admin/some/tests' ) );
		
		$this->assertFalse( Url::isAdmin( 'http://wpml.tests/some_tests/url/' ) );
	}
	
	/**
	 * @test
	 */
	public function test_login_url() {
		$this->assertTrue( Url::isLogin( 'http://wpml.tests/wp-login.php' ) );
		
		$this->assertTrue( Url::isLogin( 'http://wpml.tests/wp-login.php?param=hello' ) );
		
		$this->assertFalse( Url::isLogin( 'http://wpml.tests/test.php' ) );
	}
	
	/**
	 * @test
	 */
	public function test_content_dir() {
		$this->assertTrue( Url::isContentDirectory( 'http://wpml.tests/wp-content' ) );
		
		$this->assertTrue( Url::isContentDirectory( 'http://wpml.tests/wp-content/uploads/image.png' ) );
		
		$this->assertFalse( Url::isContentDirectory( 'http://wpml.tests/wp-includes' ) );
	}
	
}
