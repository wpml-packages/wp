<?php

namespace WPML\LIB\WP;

use WPML\FP\Fns;
use WPML\FP\Str;
use function WPML\FP\partial;

/**
 * @group option
 */
class TestOption extends \OTGS_TestCase {

	use OptionMock;
	use WPDBMock;

	public function setUp() {
		parent::setUp();

		$this->setUpOptionMock();
		$this->setUpWPDBMock();
	}

	/**
	 * @test
	 */
	public function itUpdatesAndGetsAndDeleteOption() {
		$name  = 'my-option';
		$value = 'something';

		$get    = partial( Option::get(), $name );
		$update = Option::update( $name );
		$delete = partial( Option::delete(), $name );

		$this->assertFalse( $get() );

		$this->assertTrue( $update( $value ) );
		$this->assertTrue( $this->options[ $name ]['autoload'] );

		$this->assertEquals( $value, $get() );

		$this->assertTrue( $delete() );

		$this->assertFalse( $get() );
	}

	/**
	 * @test
	 */
	public function itGetsDefaultValueIfOptionDoesNotExist() {
		$name    = 'my-option';
		$default = 'the default value';
		$value   = 'something';

		$getOr  = partial( Option::getOr(), $name );
		$update = Option::update( $name );

		$this->assertEquals( $default, $getOr( $default ) );

		$update( $value );

		$this->assertEquals( $value, $getOr( $default ) );
	}

	/**
	 * @test
	 */
	public function itUpdatesWithoutAutoload() {
		$name  = 'my-option';
		$value = 'something';

		$updateWithoutAutoLoad = Option::updateWithoutAutoLoad( $name );

		$this->assertTrue( $updateWithoutAutoLoad( $value ) );
		$this->assertFalse( $this->options[ $name ]['autoload'] );

		$this->assertEquals( $value, Option::get( $name ) );
	}

	/**
	 * @test
	 */
	public function itUpdatesAndReturnsFalseIfValueWasNotUpdated() {
		$name  = 'my-option';
		$value = 'something';

		$update = Option::update( $name );

		$this->assertTrue( $update( $value ) );
		$this->assertFalse( $update( $value ) );
	}

	public function getOrAttemptRecoveryDataProvider() {
		return [
			'Option has a proper object.' => [
				'default' => [],
				'dbValue' => null, //should not be required to return the proper value in this case.
				'optionValue' => ['object' => '\'value\''],
				'expectedValue' => ['object' => '\'value\''],
			],
			'Option has a proper string serialized object.' => [
				'default' => [],
				'dbValue' => 's:6:"serializedstring";', //should not be required to return the proper value in this case.
				'optionValue' => 'serializedstring',
				'expectedValue' => 'serializedstring',
			],
			'Option has a corrupted, recoverable string serialized object.' => [
				'default' => [],
				'dbValue' => 's:6:"serializedstrinddg";', //should not be required to return the proper value in this case.
				'optionValue' => false,
				'expectedValue' => 'serializedstrinddg',
			],
			'Option is a string representing a corrupted, recoverable serialized option.' => [
				'default' => [],
				'dbValue' => 'a:1:{s:2:"object";s:5:"value";}',
				'optionValue' => false, //this is what WP returns in such case
				'expectedValue' => [ 'object' => 'value' ],
			],
			'Option is a string representing a corrupted, alt, recoverable serialized option.' => [
				'default' => [],
				'dbValue' => 'a:1:{s:6:\'object\';s:5:"value";}',
				'optionValue' => false, //this is what WP returns in such case
				'expectedValue' => [ 'object' => 'value' ],
			],
			'Option is a string representing a corrupted, unrecoverable serialized option.' => [
				'default' => [],
				'dbValue' => 'a:1:{s:2:2;s:5:"value";}',
				'optionValue' => false, //this is what WP returns in such case
				'expectedValue' => [],
			],
		];
	}

	/**
	 * @test
	 * @dataProvider getOrAttemptRecoveryDataProvider
	 *
	 * @param mixed $default
	 * @param mixed $dbValue
	 * @param mixed $optionValue
	 * @param mixed $expectedValue
	 */
	public function itGetsOrAttemptRecovery( $default, $dbValue, $optionValue, $expectedValue ) {
		$name    = 'my-option';

		$this->addPrepareHandler(
			function ( $sql, $args ) use ( $name ) {
				return Str::includes( 'SELECT option_value FROM', $sql )
				       && Str::includes( "WHERE option_name = %s", $sql )
				       && $args === $name;
			},
			Fns::always( $dbValue )
		);

		Option::update($name, $optionValue );

		\WP_Mock::userFunction( 'is_serialized', [
			'return' => true,
		] );

		\WP_Mock::userFunction( 'maybe_unserialize', [
			'return' => function( $value ) {
				return @unserialize($value);
			}
		] );

		$value = Option::getOrAttemptRecovery( $name, $default );

		$this->assertEquals( $expectedValue, $value );
	}
}
