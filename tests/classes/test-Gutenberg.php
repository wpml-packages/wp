<?php

namespace WPML\LIB\WP;

class Test_Gutenberg extends \OTGS_TestCase {

	/**
	 * @test
	 */
	public function test_has_block() {
		$this->assertFalse( Gutenberg::hasBlock( 'some content without block') );
		$this->assertTrue( Gutenberg::hasBlock( '<!-- wp:paragraph --><p>Some content on the page.</p><!-- /wp:paragraph -->') );
	}

	/**
	 * @test
	 */
	public function test_does_not_have_block() {
		$this->assertTrue( Gutenberg::doesNotHaveBlock( 'some content without block') );
		$this->assertFalse( Gutenberg::doesNotHaveBlock( '<!-- wp:paragraph --><p>Some content on the page.</p><!-- /wp:paragraph -->') );
	}

	/**
	 * @test
	 */
	public function itShouldStripBlockData() {
		$content = '
<h1>The main title</h1>
<!-- wp:paragraph {"foo":"bar"} -->
<p>The big paragraph</p>	
<!-- /wp:paragraph -->
<!-- /wp:auto-closing {"foo":"bar"} -->
<!-- NON-BLOCK COMMENT -->
Some content with not HTML tags
<!--
	wp:paragraph {"foo":"bar"}
	-->
<p>Another paragraph</p>
<!--     /wp:paragraph    -->
';

		$expectedContent = '
<h1>The main title</h1>

<p>The big paragraph</p>	


<!-- NON-BLOCK COMMENT -->
Some content with not HTML tags

<p>Another paragraph</p>

';

		$this->assertSame( $expectedContent, Gutenberg::stripBlockData( $content ) );
	}
}
