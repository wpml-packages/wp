<?php

namespace WPML\LIB\WP;

use mocks\WordPressMock;
use WPML\FP\Either;

class Test_WordPress extends \OTGS_TestCase {
	use WordPressMock;

	public function setUp() {
		parent::setUp();

		$this->setUpWordpressMock();
	}


	/**
	 * @test
	 */
	public function it_compares_WP_versions() {
		global $wp_version;

		$wp_version = '5.0.0';

		$this->assertTrue( WordPress::versionCompare( '=', '5.0.0' ) );
		$this->assertTrue( WordPress::versionCompare( '<', '5.0.1' ) );
		$this->assertTrue( WordPress::versionCompare( '>', '4.9.5' ) );
		$this->assertTrue( WordPress::versionCompare( '>=', '5.0.0' ) );
		$this->assertTrue( WordPress::versionCompare( '<=', '5.0.0' ) );
	}

	/**
	 * @test
	 */
	public function it_handles_wp_error() {
		$handleError = WordPress::handleError();

		$error = $this->getMockBuilder( '\WP_Error' )->getMock();

		$this->assertEquals( Either::left( $error ), $handleError( $error ) );

		$correctValue = 'some value';
		$this->assertEquals( Either::right( $correctValue ), $handleError( $correctValue ) );
	}
}
