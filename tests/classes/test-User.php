<?php

namespace WPML\LIB\WP;

class Test_User extends \OTGS_TestCase {

	use UserMock;

	public function setUp() {
		parent::setUp();

		$this->setUpUserMock();
	}

	/**
	 * @test
	 */
	public function it_gets_user_id() {
		$userId = 123;

		$this->mockCurrentUserId( $userId );

		$this->assertEquals( $userId, User::getCurrentId() );
	}

	/**
	 * @test
	 */
	public function it_gets_user() {
		$user = User::getCurrent();
		$this->assertInstanceOf( 'WP_User', $user );
	}

	/**
	 * @test
	 */
	public function it_updates_and_gets() {
		$userId    = 123;
		$metaKey   = 'key';
		$metaValue = 'some data';

		User::updateMeta( $userId, $metaKey, $metaValue );

		$this->assertEquals( $metaValue, User::getMetaSingle( $userId, $metaKey ) );

		$newValue = 456;
		User::updateMeta( $userId, $metaKey, $newValue );

		$this->assertEquals( $newValue, User::getMetaSingle( $userId, $metaKey ) );
	}

	/**
	 * @test
	 */
	public function it_handles_caps() {
		$cap = 'test-cap';

		$user = User::getCurrent();
		$this->assertFalse( $user->has_cap( $cap ) );
		$user->add_cap( $cap );
		$this->assertTrue( $user->has_cap( $cap ) );
		$user->remove_cap( $cap );
		$this->assertFalse( $user->has_cap( $cap ) );
	}

	/**
	 * @test
	 */
	public function it_gets_user_by_id() {
		$userId = 123;

		$user = User::get( $userId );
		$this->assertInstanceOf( 'WP_User', $user );
	}

	/**
	 * @test
	 */
	public function it_deletes_meta() {
		$userId    = 123;
		$metaKey   = 'key';
		$metaValue = 'some data';

		User::updateMeta( $userId, $metaKey, $metaValue );

		$this->assertEquals( $metaValue, User::getMetaSingle( $userId, $metaKey ) );

		User::deleteMeta( $userId, $metaKey );

		$this->assertEquals( '', User::getMetaSingle( $userId, $metaKey ) );
	}

	/**
	 * @test
	 */
	public function it_inserts_user() {
		$data   = [ 'anything' ];
		$userId = User::insert( $data );
		$this->assertTrue( $userId > 0 );
	}

	/**
	 * @test
	 */
	public function it_notifies_user() {
		$userId = 123;
		User::notifyNew( $userId );
		$this->assertNotifiedNew( $userId );
	}

	/**
	 * @test
	 */
	public function it_adds_avatar() {
		$user = (object) [ 'ID' => 123 ];

		$result = User::withAvatar( $user );
		$this->assertEquals( get_avatar( $user->ID ), $result->avatar );
		$this->assertEquals( get_avatar_url( $user->ID ), $result->avatarUrl );
	}

	/**
	 * @test
	 */
	public function it_adds_edit_link() {
		$user = (object) [ 'ID' => 123 ];

		\WP_Mock::userFunction( 'add_query_arg', [
			'return' => function( $key, $value, $url ) {
				return "$url?$key=$value";
			}
		] );
		$_SERVER['REQUEST_URI'] = 'http://test.com';

		$result = User::withEditLink( $user );
		$this->assertEquals( "user-edit.php?user_id={$user->ID}?wp_http_referer=http%3A%2F%2Ftest.com", $result->editLink );
	}

	/**
	 * @test
	 */
	public function hasCap() {
		// Current user has no cap 'manage_options' cap.
		$this->assertFalse( User::hasCap( User::CAP_MANAGE_OPTIONS ) );

		// Add 'manage_options' cap to current user.
		$user = User::getCurrent();
		$user->add_cap( User::CAP_MANAGE_OPTIONS );
		$this->assertTrue( User::hasCap( User::CAP_MANAGE_OPTIONS ) );

		// The current user is not used when an explicit user is passed.
		$user = \Mockery::mock( 'WP_User' );
		$user->shouldReceive( 'has_cap' )->andReturn( false );
		$this->assertFalse( User::hasCap( User::CAP_MANAGE_OPTIONS, $user ) );
	}

	/**
	* @test
	* @dataProvider data_hasCap_aliases
	*/
	public function hasCap_aliases( callable $aliasFunction, $cap ) {
		// Current user has not the $cap capability.
		$this->assertFalse( $aliasFunction() );

		// Add $cap capability to current user.
		$user = User::getCurrent();
		$user->add_cap( $cap );
		$this->assertTrue( $aliasFunction() );

		// The current user is not used when an explicit user is passed.
		$user = \Mockery::mock( 'WP_User' );
		$user->shouldReceive( 'has_cap' )->andReturn( false );
		$this->assertFalse( $aliasFunction( $user ) );
	}

	public function data_hasCap_aliases() {
		return [
			'canManageTranslations' => [
				[ User::class, 'canManageTranslations' ],
				User::CAP_MANAGE_TRANSLATIONS
			],
			'canManageTranslations' => [
				[ User::class, 'canManageOptions' ],
				User::CAP_MANAGE_OPTIONS
			]
		];
	}

	/**
	 * @test
	 */
	public function it_detects_is_editor() {
		$user = User::getCurrent();
		$this->assertFalse( User::isEditor( $user ) );

		$user->add_cap( User::CAP_PUBLISH_PAGES );
		$this->assertFalse( User::isEditor( $user ) );

		$user->add_cap( User::CAP_PUBLISH_POSTS );
		$this->assertFalse( User::isEditor( $user ) );

		$user->add_cap( User::CAP_EDIT_PAGES );
		$this->assertFalse( User::isEditor( $user ) );

		$user->add_cap( User::CAP_EDIT_POSTS );
		$this->assertFalse( User::isEditor( $user ) );

		$user->add_cap( User::CAP_EDIT_OTHERS_POSTS );
		$this->assertTrue( User::isEditor( $user ) );
	}
}
