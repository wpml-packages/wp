<?php

namespace WPML\LIB\WP;


use mocks\WordPressMock;
use WPML\FP\Fns;

class Test_Http extends \OTGS_TestCase {
	use WordPressMock;

	public function setUp() {
		parent::setUp();

		$this->setUpWordpressMock();
	}


	/**
	 * @test
	 */
	public function postReturnsLeftWhenWPErrorAppears() {
		$url  = 'http://some-api..com';
		$args = [ 'body' => [ 'param1' => 'val1', 'param2' => 'val2' ] ];

		$error = $this->getMockBuilder( '\WP_Error' )->getMock();
		$http  = $this->getWPHttp();
		$http->expects( $this->once() )->method( 'post' )->with( $url, $args )->willReturn( $error );

		\WP_Mock::userFunction( 'WPML\Container\make', [ 'return' => $http ] );

		$response = Http::post( $url, $args );
		$this->assertTrue( Fns::isLeft( $response ) );
		$this->assertSame( $error, $response->orElse( Fns::identity() )->get() );
	}

	/**
	 * @test
	 */
	public function postReturnsRight() {
		$url  = 'http://some-api..com';
		$args = [ 'body' => [ 'param1' => 'val1', 'param2' => 'val2' ] ];

		$data = [ 'var1', 'var2' ];
		$http = $this->getWPHttp();
		$http->expects( $this->once() )->method( 'post' )->with( $url, $args )->willReturn( $data );

		\WP_Mock::userFunction( 'WPML\Container\make', [ 'return' => $http ] );

		$response = Http::post( $url, $args );
		$this->assertTrue( Fns::isRight( $response ) );
		$this->assertEquals( $data, $response->get() );
	}

	private function getWPHttp() {
		return $this->getMockBuilder( '\WP_Http' )->setMethods( [ 'post' ] )->getMock();
	}
}