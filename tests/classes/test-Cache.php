<?php

namespace WPML\LIB\WP;

use WPML\FP\Fns;
use WPML\FP\Just;
use WPML\FP\Logic;
use WPML\FP\Lst;
use WPML\FP\Nothing;
use WPML\FP\Relation;

class Test_Cache extends \OTGS_TestCase {

	use CacheMock;

	public function setUp() {
		parent::setUp();

		$this->setUpCache();
	}

	/**
	 * @test
	 */
	public function it_caches() {

		$group = 'my-group';
		$key = 'my-key';
		$value = 'my-value';

		Cache::set( $group, $key, 3600, $value );

		$result = Cache::get( $group, $key );
		$this->assertInstanceOf( Just::class, $result );
		$this->assertEquals( $value, $result->get() );

		$this->assertEquals( [ $key ], Cache::getKeysInGroup( $group ) );

		$this->assertInstanceOf( Nothing::class, Cache::get( 'wrong group', $key ) );
		$this->assertInstanceOf( Nothing::class, Cache::get( $group, 'wrong key' ) );

	}

	/**
	 * @test
	 * @depends it_caches
	 */
	public function it_flushes_group() {
		Cache::set( 'group1', 'key1', 3600, 1 );
		Cache::set( 'group1', 'key2', 3600, 2 );
		Cache::set( 'group1', 'key3', 3600, 3 );

		Cache::set( 'group2', 'key1', 3600, 4 );
		Cache::set( 'group2', 'key10', 3600, 5 );

		$result = Cache::get( 'group1', 'key1' );
		$this->assertInstanceOf( Just::class, $result );
		$this->assertEquals( 1, $result->get() );
		$this->assertEquals( [ 'key1', 'key2', 'key3' ], Cache::getKeysInGroup( 'group1' ) );

		$result = Cache::get( 'group2', 'key1' );
		$this->assertInstanceOf( Just::class, $result );
		$this->assertEquals( 4, $result->get() );
		$this->assertEquals( [ 'key1', 'key10' ], Cache::getKeysInGroup( 'group2' ) );

		Cache::flushGroup( 'group1' );

		$result = Cache::get( 'group1', 'key1' );
		$this->assertInstanceOf( Nothing::class, $result );
		$this->assertEquals( [], Cache::getKeysInGroup( 'group1' ) );

		$result = Cache::get( 'group2', 'key1' );
		$this->assertInstanceOf( Just::class, $result );
		$this->assertEquals( 4, $result->get() );
		$this->assertEquals( [ 'key1', 'key10' ], Cache::getKeysInGroup( 'group2' ) );
	}

	/**
	 * @test
	 */
	public function it_memorizes() {

		$callCount = 0;

		$group = 'my-group';

		$fn = function( $arg1, $arg2 ) use ( &$callCount ) {
			$callCount++;
			return Lst::makePair( $arg1, $arg2 );
		};

		$memorize = Cache::memorize( $group, 3600, $fn );
		$this->assertEquals( Lst::makePair( 1, 2 ), $memorize( 1, 2 ) );
		$this->assertEquals( 1, $callCount );

		$this->assertEquals( Lst::makePair( 1, 2 ), $memorize( 1, 2 ) );
		$this->assertEquals( 1, $callCount );

		$this->assertEquals( Lst::makePair( 2, 2 ), $memorize( 2, 2 ) );
		$this->assertEquals( 2, $callCount );

		$this->assertEquals( Lst::makePair( 2, 2 ), $memorize( 2, 2 ) );
		$this->assertEquals( 2, $callCount );

	}

	/**
	 * @test
	 */
	public function it_memorizes_null_values() {

		$callCount = 0;

		$group = 'my-group';

		$fn = function( $arg1, $arg2 ) use ( &$callCount ) {
			$callCount++;
			return null;
		};

		$memorize = Cache::memorize( $group, 3600, $fn );
		$this->assertEquals( null, $memorize( 1, 2 ) );
		$this->assertEquals( 1, $callCount );

		$this->assertEquals( null, $memorize( 1, 2 ) );
		$this->assertEquals( 1, $callCount );

		$this->assertEquals( null, $memorize( 2, 2 ) );
		$this->assertEquals( 2, $callCount );

		$this->assertEquals( null, $memorize( 2, 2 ) );
		$this->assertEquals( 2, $callCount );

	}

	/**
	 * @test
	 */
	public function it_memorizes_with_check() {

		$callCount = 0;

		$group = 'my-group';

		$fn = function( $arg1, $arg2 ) use ( &$callCount ) {
			$callCount++;
			return $arg1 + $arg2;
		};

		$checkingFn = Relation::gt( Fns::__, 10 );

		$memorizeWithCheck = Cache::memorizeWithCheck( $group )( $checkingFn )( 3600 )( $fn );
		$this->assertEquals( 11, $memorizeWithCheck( 9, 2 ) );
		$this->assertEquals( 1, $callCount );

		$this->assertEquals( 11, $memorizeWithCheck( 9, 2 ) );
		$this->assertEquals( 1, $callCount );

		$this->assertEquals( 9, $memorizeWithCheck( 7, 2 ) );
		$this->assertEquals( 2, $callCount );

		$this->assertEquals( 9, $memorizeWithCheck( 7, 2 ) );
		$this->assertEquals( 3, $callCount );

		$this->assertEquals( 9, $memorizeWithCheck( 7, 2 ) );
		$this->assertEquals( 4, $callCount );


	}

	/**
	 * @test
	 */
	public function it_deletes_cached_value() {
		$group = 'my-group';
		Cache::set( $group, 'key1', 3600, 'val1' );
		Cache::set( $group, 'key2', 3600, 'val2' );

		$this->assertEquals( [ 'key1', 'key2' ], Cache::getKeysInGroup( $group ) );
		$this->assertEquals( 'val1', Cache::get( $group, 'key1' )->getOrElse( null ) );

		Cache::delete( $group, 'key1' );
		$this->assertEquals( [ 'key2' ], Cache::getKeysInGroup( $group ) );
		$this->assertEquals( null, Cache::get( $group, 'key1' )->getOrElse( null ) );
	}

	/**
	 * @test
	 */
	public function it_clears_memoized_function_call() {
		$add = function ( $a, $b ) {
			return $a + $b;
		};

		$group = 'my-group';

		$memorize = Cache::memorize( $group, 3600, $add );
		$this->assertEquals( 3, $memorize( 1, 2 ) );
		$this->assertEquals( 5, $memorize( 1, 4 ) );
		$this->assertEquals( 6, $memorize( 3, 3 ) );

		// Check that the cache is populated
		$this->assertEquals( array_map( 'serialize', [ [ 1, 2 ], [ 1, 4 ], [ 3, 3 ] ] ), Cache::getKeysInGroup( $group ) );
		$this->assertEquals( 5, Cache::get( $group, Cache::_buildKeyForFunctionArguments( [ 1, 4 ] ) )->getOrElse( null ) );

		Cache::clearMemoizedFunction( $group, 1, 4 );
		// Check that the cache is cleared
		$this->assertEquals( array_map( 'serialize', [ [ 1, 2 ], [ 3, 3 ] ] ), Cache::getKeysInGroup( $group ) );
		$this->assertEquals( null, Cache::get( $group, Cache::_buildKeyForFunctionArguments(( [ 1, 4 ] ) ) )->getOrElse( null ) );
	}
}
