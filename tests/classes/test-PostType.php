<?php

namespace WPML\LIB\WP;

use WPML\FP\Maybe;

class Test_PostType extends \OTGS_TestCase {

	use PostTypeMock;

	public function setUp() {
		parent::setUp();

		$this->setUpPostTypeMock();
	}

	/**
	 * @test
	 */
	public function it_gets_posts_count() {
		$this->mockPublishedPostsCount( 'post', 5 );
		$this->mockPublishedPostsCount( 'page', 2 );

		$this->assertEquals( 5, PostType::getPublishedCount( 'post' ) );
		$this->assertEquals( 2, PostType::getPublishedCount( 'page' ) );
		$this->assertEquals( 0, PostType::getPublishedCount( 'attachment' ) );
	}

	/**
	 * @test
	 */
	public function it_gets_plural() {
		$this->assertEquals( Maybe::of( 'Posts' ), PostType::getPluralName( 'post' ) );
		$this->assertEquals( Maybe::of( 'Pages' ), PostType::getPluralName( 'page' ) );
		$this->assertEquals( Maybe::nothing(), PostType::getPluralName( 'non-existing' ) );
	}

	/**
	 * @test
	 */
	public function it_gets_singular() {
		$this->assertEquals( Maybe::of( 'Post' ), PostType::getSingularName( 'post' ) );
		$this->assertEquals( Maybe::of( 'Page' ), PostType::getSingularName( 'page' ) );
		$this->assertEquals( Maybe::nothing(), PostType::getSingularName( 'non-existing' ) );
	}

	/**
	 * @test
	 */
	public function it_gets_object() {
		$this->assertInstanceOf( 'WP_Post_Type', PostType::getObject( 'post' )->getOrElse(null) );
		$this->assertInstanceOf( 'WP_Post_Type', PostType::getObject( 'page' )->getOrElse(null) );
		$this->assertEquals( Maybe::nothing(), PostType::getObject( 'non-existing' ) );
	}

}
