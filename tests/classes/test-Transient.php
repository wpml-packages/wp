<?php

namespace WPML\LIB\WP;

use mocks\TransientMock;

/**
 * @group Transient
 */
class TestTransient extends \OTGS_TestCase {

	use TransientMock;

	public function setUp() {
		parent::setUp();

		$this->setUpTransientMock();
	}

	/**
	 * @test
	 */
	public function test_Transients() {
		$key = 'myKey';

		$this->assertNull( Transient::get( $key ) );
		$this->assertEquals( 123, Transient::getOr( $key, 123 ) );

		Transient::set( $key, 'some value', 3600 );
		$this->assertEquals( 'some value', Transient::get( $key ) );
		$this->assertEquals( 'some value', Transient::getOr( $key, 123 ) );

		Transient::delete( $key );
		$this->assertNull( Transient::get( $key ) );
	}
}