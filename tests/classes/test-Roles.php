<?php

namespace WPML\LIB\WP;

class Test_Roles extends \OTGS_TestCase {

	use RolesMock;

	public function setUp() {
		parent::setUp();

		$this->setupRolesMock();
	}

	/**
	 * @test
	 */
	public function test_has_caps() {
		$this->assertFalse( Roles::hasCap( 'undefined_cap', 'administrator' ) );
		$this->assertFalse( Roles::hasCap( 'manage_options', 'undefined_role' ) );

		$this->assertFalse( Roles::hasCap( 'manage_options', 'subscriber' ) );
		$this->assertTrue( Roles::hasCap( 'manage_options', 'administrator' ) );
	}

}
