<?php

namespace WPML\LIB\WP;

use WPML\FP\Fns;
use WPML\FP\Str;

class Test_Attachment extends \OTGS_TestCase {
	use WPDBMock;

	public function setUp() {
		parent::setUp();

		$this->setUpWPDBMock();
	}

	/**
	 * @test
	 */
	public function it_returns_null_if_can_not_find_attachement() {
		\WP_Mock::userFunction( 'attachment_url_to_postid', ['return' => 0] );

		$this->assertSame( null, Attachment::idFromUrl( 'any-url.png' ) );
	}

	/**
	 * @test
	 */
	public function it_finds_attachement() {
		$attachmentId = 123;

		$url = 'http://wpml.tests/wp-content/uploads/2021/06/ATE-hangs-when-editing-media-translation.png';

		\WP_Mock::userFunction( 'attachment_url_to_postid', [
			'args'   => [ $url ],
			'return' => $attachmentId,
		] );

		$this->assertEquals( $attachmentId, Attachment::idFromUrl( $url ) );
	}

	/**
	 * @test
	 */
	public function it_uses_fallback_of_unsized_image_if_it_can_not_find_one_with_size() {
		$attachmentId = 123;

		$url            = 'http://wpml.tests/wp-content/uploads/2021/06/ATE-hangs-when-editing-media-translation-1024x517.png';
		$urlWithoutSize = str_replace( '-1024x517', '', $url );

		\WP_Mock::userFunction( 'attachment_url_to_postid', [
			'args'   => [ $url ],
			'return' => 0,
		] );

		\WP_Mock::userFunction( 'attachment_url_to_postid', [
			'args'   => [ $urlWithoutSize ],
			'return' => $attachmentId,
		] );

		$this->assertEquals( $attachmentId, Attachment::idFromUrl( $url ) );
	}

	/**
	 * @test
	 */
	public function it_uses_fallback_to_guid_when_attachment_url_to_postid_returns_zero() {
		$attachmentId = 123;

		$url = 'http://wpml.tests/wp-content/uploads/2021/06/ATE-hangs-when-editing-media-translation.png';

		\WP_Mock::userFunction( 'attachment_url_to_postid', [ 'return' => 0 ] );

		$this->addPrepareHandler(
			function ( $sql, $args ) use ( $url ) {
				return Str::includes( 'SELECT ID FROM', $sql )
				       && Str::includes( "WHERE guid='%s'", $sql )
				       && $args === $url;
			},
			Fns::always( [ $attachmentId ] )
		);

		$this->assertEquals( $attachmentId, Attachment::idFromUrl( $url ) );
	}

	/**
	 * @test
	 */
	public function it_uses_size_fallback_to_guid_when_attachment_url_to_postid_returns_zero() {
		$attachmentId = 123;

		$url            = 'http://wpml.tests/wp-content/uploads/2021/06/ATE-hangs-when-editing-media-translation-1024x517.png';
		$urlWithoutSize = str_replace( '-1024x517', '', $url );

		\WP_Mock::userFunction( 'attachment_url_to_postid', [ 'return' => 0 ] );

		$this->addPrepareHandler(
			function ( $sql, $args ) use ( $urlWithoutSize ) {
				return Str::includes( 'SELECT ID FROM', $sql )
				       && Str::includes( "WHERE guid='%s'", $sql )
				       && $args === $urlWithoutSize;
			},
			Fns::always( [ $attachmentId ] )
		);

		$this->assertEquals( $attachmentId, Attachment::idFromUrl( $url ) );
	}

}
