<?php

namespace WPML\LIB\WP;

/**
 * @group wpdb
 */
class Test_WPDB extends \OTGS_TestCase {

	use WPDBMock;

	public function setUp() {
		parent::setUp();
		$this->setUpWPDBMock();
	}

	/**
	 * @test
	 * @dataProvider dpOriginalSuppressErrors
	 *
	 * @param bool $originalSuppressErrors
	 */
	public function itShouldRunQueryWithoutErrors( $originalSuppressErrors ) {
		/** @var \wpdb $wpdb */
		global $wpdb;

		$query = 'SOME SQL QUERY';

		$getCol = function() use ( $wpdb, $query ) {
			return $wpdb->get_col( $query );
		};

		$wpdb->suppress_errors = $originalSuppressErrors;

		$this->assertEquals( $query, WPDB::withoutError( $getCol ) );
		$this->assertSame( $originalSuppressErrors, $wpdb->suppress_errors );
	}

	public function dpOriginalSuppressErrors() {
		return [
			[ true ],
			[ false ],
		];
	}
}
