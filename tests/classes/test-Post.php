<?php

namespace WPML\LIB\WP;

use WPML\FP\Fns;
use WPML\FP\Obj;

class Test_Post extends \OTGS_TestCase {
	use WPDBMock;
	use PostMock;

	public function setUp() {
		parent::setUp();

		$this->setUpWPDBMock();
		$this->setUpPostMock();
	}

	/**
	 * @test
	 */
	public function it_gets_terms_from_post() {
		$postId   = 123;
		$taxonomy = 'taxonomy';
		$term     = \Mockery::mock( 'WP_Term' );

		$this->setTermsToPost( $postId, $taxonomy, [ $term ] );
		$this->assertEquals( [ $term ], Post::getTerms( $postId, $taxonomy )->get() );
	}

	/**
	 * @test
	 */
	public function test_get_terms_returns_left_on_error() {
		$postId   = 123;
		$taxonomy = 'taxonomy';

		$this->setTermsToPost( $postId, $taxonomy, \Mockery::mock( 'WP_Error' ) );
		$this->assertInstanceOf( 'WP_Error', Post::getTerms( $postId, $taxonomy )->orElse( Fns::identity() )->get() );
	}

	/**
	 * @test
	 */
	public function test_get_terms_returns_left_on_false() {
		$postId   = 123;
		$taxonomy = 'taxonomy';

		$this->setTermsToPost( $postId, $taxonomy, false );
		$this->assertFalse( Post::getTerms( $postId, $taxonomy )->orElse( Fns::identity() )->get() );
	}

	/**
	 * @test
	 */
	public function it_gets_post_meta() {
		$postId  = 123;
		$metaKey = 'xyz';
		$data    = 'some data';

		Post::updateMeta( $postId, $metaKey, $data );

		$this->assertEquals( $data, Post::getMetaSingle( $postId, $metaKey ) );

	}

	/**
	 * @test
	 */
	public function it_deletes_post_meta() {
		$postId  = 123;
		$metaKey = 'xyz';
		$data    = 'some data';

		Post::updateMeta( $postId, $metaKey, $data );
		Post::deleteMeta( $postId, $metaKey );

		$this->assertEquals( '', Post::getMetaSingle( $postId, $metaKey ) );
	}

	/**
	 * @test
	 */
	public function it_gets_post_type() {
		$existingPostId    = 123;
		$nonExistingPostId = 456;
		$type              = 'page';

		$this->mockPostType( $existingPostId, $type );

		$this->assertEquals( $type, Post::getType( $existingPostId ) );

		$this->assertEquals( false, Post::getType( $nonExistingPostId ) );
	}

	/**
	 * @test
	 */
	public function it_gets_post() {
		$existingPostId    = 123;
		$nonExistingPostId = 456;

		$this->mockPost( $existingPostId );

		$this->assertEquals( $this->createPost( $existingPostId ), Post::get( $existingPostId ) );

		$this->assertNull( Post::get( $nonExistingPostId ) );
	}

	/**
	 * @test
	 */
	public function it_gets_post_status() {
		$existingPostId    = 123;
		$nonExistingPostId = 456;
		$status            = 'published';

		$this->mockPostStatus( $existingPostId, $status );

		$this->assertEquals( $status, Post::getStatus( $existingPostId ) );

		$this->assertEquals( false, Post::getStatus( $nonExistingPostId ) );
	}

	/**
	 * @test
	 */
	public function it_updates_post() {
		$postId = 12;
		$this->mockPost( $postId );

		$post = Post::get( $postId );
		$this->assertEquals( 'publish', Obj::prop( 'post_status', $post ) );

		$this->assertEquals( $postId, Post::update( [ 'ID' => $postId, 'post_status' => 'draft' ] ) );
		$post = Post::get( $postId );
		$this->assertEquals( 'draft', Obj::prop( 'post_status', $post ) );


		$this->assertEquals( 0, Post::update( [ 'ID' => 789, 'post_status' => 'draft' ] ) );
	}

	/**
	 * @test
	 */
	public function it_inserts_post() {
		$data = [ 'post_title' => 'My post 1', 'post_content' => 'my content 1', 'post_type' => 'post' ];
		$id   = Post::insert( $data );
		$this->assertEquals( 1, $id );

		$post = Post::get( $id );
		$this->assertEquals( $data['post_title'], $post->post_title );
		$this->assertEquals( $data['post_content'], $post->post_content );
		$this->assertEquals( 'publish', $post->post_status );
		$this->assertEquals( 'post', $post->post_type );

		$this->mockPost( 3, [] );

		$data = [ 'post_title' => 'My page 1', 'post_content' => 'my page content 1' ];
		$id   = Post::insert( $data );
		$this->assertEquals( 4, $id );

		$post = Post::get( $id );
		$this->assertEquals( $data['post_title'], $post->post_title );
		$this->assertEquals( $data['post_content'], $post->post_content );
		$this->assertEquals( 'publish', $post->post_status );
		$this->assertEquals( 'page', $post->post_type );
	}

	/**
	 * @test
	 */
	public function it_sets_status() {
		$postId = 12;
		$this->mockPost( $postId );

		$post = Post::get( $postId );
		$this->assertEquals( 'publish', Obj::prop( 'post_status', $post ) );

		$setStatus = Post::setStatus( Fns::__, 'draft' );
		$this->assertEquals( $postId, $setStatus( $postId ) );
		$post = Post::get( $postId );
		$this->assertEquals( 'draft', Obj::prop( 'post_status', $post ) );
	}

	/**
	 * @test
	 */
	public function it_sets_status_without_filters() {
		$postId = 12;
		$this->mockPost( $postId );

		$post = Post::get( $postId );
		$this->assertEquals( 'publish', Obj::prop( 'post_status', $post ) );

		\WP_Mock::userFunction( 'clean_post_cache', [
			'args'   => [ $postId ],
			'return' => 0,
		] );
		$setStatus = Post::setStatusWithoutFilters( Fns::__, 'draft' );
		$this->assertEquals( $postId, $setStatus( $postId ) );
		$post = Post::get( $postId );
		$this->assertEquals( 'draft', Obj::prop( 'post_status', $post ) );
	}

	/**
	 * @test
	 */
	public function it_deletes_post() {
		$postId = 12;
		$this->mockPost( $postId );

		$this->assertNotNull( Post::get( $postId ) );

		Post::delete( $postId );

		$this->assertNull( Post::get( $postId ) );
	}

	/**
	 * @test
	 */
	public function it_trashes_post() {
		$postId = 12;
		$this->mockPost( $postId );

		$this->assertNotNull( Post::get( $postId ) );

		Post::trash( $postId );

		$this->assertNotNull( Post::get( $postId ) );

		$this->assertEquals( 'trash', Post::getStatus( $postId ) );
	}

}
