<?php

use tad\FunctionMocker\FunctionMocker;

define( 'MAIN_DIR', __DIR__ . '/..' );
define( 'WP_CONTENT_URL', 'wp-content');

include MAIN_DIR . '/vendor/autoload.php';
require_once __DIR__ . '/mocks/WPDBMock.php';
require_once __DIR__ . '/mocks/OnActionMock.php';
require_once __DIR__ . '/mocks/NonceMock.php';
require_once __DIR__ . '/mocks/SendJSONMock.php';
require_once __DIR__ . '/mocks/CacheMock.php';
require_once __DIR__ . '/mocks/PostMock.php';
require_once __DIR__ . '/mocks/PostTypeMock.php';
require_once __DIR__ . '/mocks/OptionMock.php';
require_once __DIR__ . '/mocks/TransientMock.php';
require_once __DIR__ . '/mocks/UserMock.php';
require_once __DIR__ . '/mocks/RolesMock.php';
require_once __DIR__ . '/mocks/WordPressMock.php';

FunctionMocker::init(
	[
		'whitelist' => [
			realpath( MAIN_DIR . '/classes' ),
		],
		'redefinable-internals' => [
			'function_exists',
			'file_exists',
		],
	]
);
