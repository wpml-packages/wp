<?php

namespace WPML\LIB\WP;

use WPML\FP\Promise;

class Test_OnAction extends \PHPUnit_Framework_TestCase {

	use OnActionMock;

	public function setUp() {
		parent::setUp();

		$this->setUpOnAction();
	}

	/**
	 * @test
	 */
	public function it_adds_action() {

		$actionName    = 'action-name';
		$priority      = 123;
		$accepted_args = 3;

		Hooks::onAction( $actionName, $priority, $accepted_args );

		$this->assertActionAdded( $actionName, $priority, $accepted_args );
	}

	/**
	 * @test
	 */
	public function it_returns_a_promise() {
		$this->assertInstanceOf( Promise::class, Hooks::onAction( 'my-action' ) );
	}

	/**
	 * @test
	 */
	public function it_runs_action() {
		$actionName    = 'action-name';
		$priority      = 123;
		$accepted_args = 3;
		$args          = [ 'param1', 'param2', 'param3' ];

		Hooks::onAction( $actionName, $priority, $accepted_args )
			->then( function ( $data ) use ( $args ) {
				$this->assertEquals( $args, $data );
			} );

		$this->runAction( $actionName, ...$args );
	}
}
